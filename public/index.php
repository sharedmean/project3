<?php

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');


// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
    $messages = array(); // врменные сообщения пользователю
    
    if (!empty($_COOKIE['save'])) {
    
        setcookie('save', '', 100000);
        
    print('Thank you! We saved your results');
  }
  
  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['agree'] = !empty($_COOKIE['agree_error']);
  
  // Выдаем сообщения об ошибках.
  if ($errors['fio']) {
      // Удаляем куки, указывая время устаревания в прошлом.
      setcookie('fio_error', '', 100000);
      // Выводим сообщение.
      if ($errors['fio'] == 'empty') $messages['fio'] = '<div class="errors">FIO is required.</div>';
      else $messages['fio'] = '<div class="errors">Only letters and white space allowed.</div>';
  }
  if ($errors['email']) {
      setcookie('email_error', '', 100000);
      if($errors['email'] == 'empty') $messages['email'] = '<div class="errors">Email is required.</div>';
      else $messages['email'] = '<div class="errors">Invalid email format.</div>';
  }
  if ($errors['date']) {
      setcookie('date_error', '', 100000);
      $messages['date'] = '<div class="errors">Date is required.</div>';
  }
  if ($errors['sex']) {
      setcookie('sex_error', '', 100000);
      $messages['sex'] = '<div class="errors">Sex is required.</div>';
  }
 
  if ($errors['bio']) {
      setcookie('bio_error', '', 100000);
      $messages['bio'] = '<div class="errors">Biography is required.</div>';
  }
  if ($errors['agree']) {
      setcookie('agree_error', '', 100000);
      $messages['agree'] = '<div class="errors">Agreement is required.</div>';
  }
  
  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
  $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
  $values['agree'] = empty($_COOKIE['agree_value']) ? '' : $_COOKIE['agree_value'];
  
  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
    
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;

if (empty($_POST['fio'])) {
    setcookie('fio_error', 'empty');
  $errors = TRUE;
}
else if (!preg_match('/^[a-zA-Z]+$/u', $_POST['fio'])) {
    setcookie('fio_error', 'invalid');
  $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
}



if (empty($_POST['email'])) {
    setcookie('email_error', 'empty');
    $errors = TRUE;

}
else {
    setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
}





if (empty($_POST['date'])) {
    setcookie('date_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('date_value', $_POST['date'], time() + 365 * 24 * 60 * 60);
}

switch($_POST['sex']) {
    case 'man': {
        $sex='man';
        break;
    }
    case 'woman':{
        $sex='woman';
        break;
    }
};



if(empty($_POST['agree']))
{
    setcookie('agree_error', 'empty');
    $errors = TRUE;
}




switch($_POST['limbs']) {
    case '1': {
        $limbs='1';
        break;
    }
    case '2':{
        $limbs='2';
        break;
    }
    case '3':{
        $limbs='3';
        break;
    }
    case '4':{
        $limbs='4';
        break;
    }
};





if ($errors) {
    // При наличии ошибок завершаем работу скрипта.
    exit();
}
else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('limbs_error', '', 100000);
    setcookie('bio_error', '', 100000);
    setcookie('agree_error', '', 100000);
    
}

// Сохранение в XML-документ.
// ...

// Сохраняем куки с признаком успешного сохранения.
setcookie('save', '1');



// Сохранение в базу данных.

$user = 'u20385';
$pass = '9967441';
$db = new PDO('mysql:host=localhost;dbname=u20385', $user, $pass,
  array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO application SET fio = ?, email = ?, date = ?, sex = ?, limbs = ?, bio = ?");
  $stmt->execute([$_POST['fio'],$_POST['email'],$_POST['date'], $_POST['sex'],$_POST['limbs'],$_POST['bio'],]);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage()); 
  exit();
}


header('Location: ?save=1');
